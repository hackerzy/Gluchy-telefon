#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>

typedef unsigned long int iks;
void sprawdz(iks x )
{
   if (x<=0 || x>4294967295)
   {
      printf("Przesadziles, x jest za duzy lub za maly\n");exit(0);
   }
}

iks funkcja_trzecia (iks x)
{
int i=2;
int liczba_pierwsza=0;
printf("Liczba przyjeta to: %lu\n",x);
do
{
   x++;
   liczba_pierwsza=0;
   for (i=2;i<x;i++)
      if (x%i==0)
         liczba_pierwsza++;
}
while(liczba_pierwsza>0);
return x;
}


int main(int argc , char *argv[])
{
   iks y;
   iks x;
   int fd;
   char *myfifo="/tmp/myfifo";
 char buf[1024];
   mkfifo(myfifo,0666);
   fd=open(myfifo,O_RDONLY);
   read(fd, &buf,sizeof(buf));
   x=atoll(buf);
   printf("Program trzeci\n");
   sprawdz(x);
   y=funkcja_trzecia(x);
   printf("Nastepna liczba pierwsza to: %lu\n",y);
  
return 0;
}

